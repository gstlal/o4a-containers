## Tools

* **query_influx**  - monitor gstlal_inspiral processes by looking at influxdb updates
* **checkAnalysisHttp** - monitor gstlal_inspiral processes by making HTTP requests
* **checkAnalysisHttpTimes** - command line tool to check the response times of gstlal_inspiral HTTP servers

## Notes

`query_influx` depends on the gstlal python package and so needs to be run in a container or in an environment where the gstlal package is available.

`checkAnalysisHttp` and `checkAnalysisHttpTimes` are written in Perl and have some Perl package dependencies, but they seem to be satisfied at CIT.


## Example command lines:
```
   query_influx --analysis-dir=/home/gstlalcbc.online/observing/4/a/runs/trigs.edward_o4a --scald-config=web/inspiral.yml --analysis-config=config.yml
   checkAnalysisHttp      --dir=/home/gstlalcbc.online/observing/4/a/runs/trigs.edward_o4a/ --dag=online_inspiral_edward.dag
   checkAnalysisHttpTimes --dir=/home/gstlalcbc.online/observing/4/a/runs/trigs.edward_o4a/ --dag=online_inspiral_edward.dag
```
