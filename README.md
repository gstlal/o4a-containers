# o4a-containers

Analysis containers to define production environment for GstLAL low-latency online analyses in O4.

## containers

Containers are built automatically by the CI pipeline and pushed to the container registry at containers.ligo.org. For a non-writable container, pull with:
```bash
singularity build <build-name> docker://containers.ligo.org/gstlal/o4a-containers:<analysis_tag>
```

For a writable container, pull with:
```bash
singularity build --fix-perms --sandbox <build-name> docker://containers.ligo.org/gstlal/o4a-containers:<analysis_tag>
```

### Available analysis tags are:
**main**: for a small BBH-only analysis to be run at ICDS, this is used for testing and onboarding

**edward**: for the portion of the AllSky analysis run at CIT

**jacob**: for the portion of the AllSky analysis run at ICDS

**early-warning**: for the Early Warning analysis run at CIT

**charlie**: for the portion of the SSM analysis run at ICDS

**renee**: for the portion of the AllSky analysis using pre-engineering data at CIT

**esme**: for the portion of the AllSky analysis using pre-engineering data at ICDS

**ssm_offline**: for the portion of the SSM offline analysis at all clusters




