FROM containers.ligo.org/lscsoft/gstlal:master

ARG gitlab_token
ARG gitlab_name

LABEL name "gstlal small bbh online analysis container" \
      date="2023-03-27" 

# ADD config files needed to launch an online analysis from scratch
ADD config.yml online-analysis/config.yml
ADD web/inspiral.yml online-analysis/web/inspiral.yml
ADD profiles/ics_online.yml online-analysis/profiles/ics_online.yml
ADD influx_creds.sh.sample online-analysis/influx_creds.sh

RUN mkdir -p online-analysis/bank/
RUN mkdir -p online-analysis/psd/
RUN mkdir -p online-analysis/mass_model/

# ADD any patches we need to apply manually
ADD patches/snglcoinc.patch patches/snglcoinc.patch

USER root

RUN mkdir -p src && cd src && \
    git clone https://$gitlab_name:$gitlab_token@git.ligo.org/gstlal/pastro.git && \
    cd pastro && \
    git checkout v0.0.4-mdc12 && \
    python3 setup.py install --old-and-unmanageable && \
    cd ../../

RUN mkdir -p src && cd src && \
    git clone https://$gitlab_name:$gitlab_token@git.ligo.org/chad-hanna/manifold.git && \
    cd manifold && \
    git checkout 0.0.3 && \
    python3 setup.py install --old-and-unmanageable && \
    cd ../../

RUN mkdir -p src && cd src && \
    git clone https://$gitlab_name:$gitlab_token@git.ligo.org/lscsoft/lalsuite.git && \
    cd lalsuite && \
    git checkout lalsuite-v7.13 && \
    git apply ../../patches/snglcoinc.patch && \
    ./00boot && \
    ./configure --prefix=/usr --enable-swig-python --disable-gcc-flags && \
    make && \
    make install

ENTRYPOINT bash
